import math
import pygame, sys
from game import Tablero, InteligenciaArtificial
from pygame.locals import *
import os


class GUI:
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.init()
    window = pygame.display.set_mode((700, 650))
    pygame.display.set_caption("Abalone")
    icon = pygame.image.load("../Abalone/resources/images/univalle.jpg")
    pygame.display.set_icon(icon)
    ruta = ""

    hexagono = pygame.image.load("../Abalone/resources/images/Vacio.png")
    hexagono = pygame.transform.scale(hexagono, (80, 80))
    boton = pygame.image.load("../Abalone/resources/images/refresh.png")
    boton = pygame.transform.scale(boton, (90, 90))
    boton_refresh = pygame.image.load("../Abalone/resources/images/refresh-select.png")
    boton_refresh = pygame.transform.scale(boton_refresh, (90, 90))

    fuente = pygame.font.Font(None, 50)
    label_j1 = fuente.render("Jugador 1", True, (255, 255, 255))
    label_ganador1 = fuente.render("Ganador Jugador 1", True, (255, 255, 255))
    label_j2 = fuente.render("IA", True, (0, 0, 0))
    label_ganador2 = fuente.render("Ganador IA ", True, (0, 0, 0))
    ganador = False
    turno_jugador1 = True
    obj_tablero = Tablero()
    ficha_mover = []

    def aux_pintar_tablero(self, cantidad_hexagonos, x, y):
        """
        Pinta los exagonos de cada fila
        :param cantidad_hexagonos: Cantidad de hexagonos a pintar
        :param x: posicion incial en x para pintar los hexagonos
        :param y: posicion incial en y para pintar los hexagonos
        :return: Void
        """
        for i in range(cantidad_hexagonos):
            self.window.blit(self.hexagono, (x, y))
            x += 65

    def pintar_tablero(self):
        """
        Se pinta los hexagonos necesarios para simular el tablero de juego
        :return:
        """
        aux_x = 32
        aux_y = 60
        pos_x = 170
        pos_y = 80
        for value in range(5, 10):
            self.aux_pintar_tablero(value, pos_x, pos_y)
            pos_x -= aux_x
            pos_y += aux_y
        iterator = 8
        pos_x += (2 * aux_x)
        for value in range(4):
            self.aux_pintar_tablero(iterator - value, pos_x, pos_y)
            pos_x += aux_x
            pos_y += aux_y

    def pintar_fichas(self):
        """
        Lee la matriz ubicacion_fichas y va pintado las fichas 'B' | 'N' en su respectiva posicion y color
        :return:
        """
        for fila in self.obj_tablero.ubicación_fichas:
            for columna in fila:
                if columna == "-":
                    pass
                elif columna.get_tipo() == "O":
                    pass
                else:
                    pygame.draw.circle(self.window, columna.get_color(), (columna.get_coordenadas()[0],
                                                                          columna.get_coordenadas()[1]), 30)

    def aux(self, fila):
        """
        A partir de la fila numero 5, la cantidad de casillas va disminuyendo. Esta funcion determina cuantos
        hexagonos hay que pintar
        :param fila: fila del tablero de juego
        :return: Cantidad de casillas en una determinada fila
        """
        if fila == 5:
            return 8
        elif fila == 6:
            return 7
        elif fila == 7:
            return 6
        else:
            return 5

    def ficha_seleccionada(self, fila, pos_x):
        """
        Identifica la posicion[fila][columna] de la ficha en la matriz a partir de las coordenas x,y
        obtenidas del metodo del mouse.
        El metodo seleccionada de la clase Ficha, determina si el clic hecho por el mouse esta dentro de la ficha.
        Se valida que la ficha seleccionada pertenezca al jugador del turno actual (Jugador1:Blancas | Jugador2:Negras)
        :param fila: Fila donde probablente este la ficha
        :param pos_x: Posicion en x captada por el evento del mouse
        :return: list [Fila, Columna]
        """
        pos_seleccionada = [0, 0]
        if fila < 5:
            espacios = fila + 5
            for columna in range(espacios):
                if self.obj_tablero.ubicación_fichas[fila + 4][columna].seleccionada(pos_x):
                    pos_seleccionada[0] = fila + 4
                    pos_seleccionada[1] = columna
                    break
                else:
                    pass
                fila -= 1
        else:
            """ Las casillas de las ultimas filas del tablero van disminuyendo, para obtener este numero de casillas se
            usa el metodo aux()"""
            espacios = self.aux(fila)
            iterador_fila = 8
            columna = fila - 4
            for val in range(espacios):
                if self.obj_tablero.ubicación_fichas[iterador_fila][columna].seleccionada(pos_x):
                    pos_seleccionada[0] = iterador_fila
                    pos_seleccionada[1] = columna
                    break
                else:
                    pass
                iterador_fila -= 1
                columna += 1

        if self.turno_jugador1:
            if self.obj_tablero.tablero[pos_seleccionada[0]][pos_seleccionada[1]] == "B" or \
                    self.obj_tablero.tablero[pos_seleccionada[0]][pos_seleccionada[1]] == "O":
                return pos_seleccionada
            else:
                return []
        else:
            if self.obj_tablero.tablero[pos_seleccionada[0]][pos_seleccionada[1]] == "N" or \
                    self.obj_tablero.tablero[pos_seleccionada[0]][pos_seleccionada[1]] == "O":
                return pos_seleccionada
            else:
                return []

    def pos_not_available(self, ficha_inicial, ficha_destino):
        """
        Valida que la ficha que va a ser empujada, se encuentre al rededor de la ficha seleccionada inicialmente, que su
        distancia sea 1
        :param ficha_inicial: coordenadas[Fila, Columna] de la ficha seleccionada inicialmente
        :param ficha_destino: coordenadas[Fila, Columna] de la ficha que se pretende empujar
        :return: Boolean
        """
        distancia = round(
            math.sqrt((ficha_destino[1] - ficha_inicial[1]) ** 2 + (ficha_destino[0] - ficha_inicial[0]) ** 2))
        anterior = [ficha_inicial[0] - 1, ficha_inicial[1] - 1]
        posteior = [ficha_inicial[0] + 1, ficha_inicial[1] + 1]
        if anterior == ficha_destino or posteior == ficha_destino:
            """ Debido a la forma del tablero de juego, en la matriz en la que se almacena la informacion, hay dos casillas
            que se encuentran al rededor de una determinada casilla, pero en el tablero de juego no lo estan (anterior &
            posterior)"""
            return False
        elif distancia != 1:
            return False
        else:
            return True

    def almacenar_ubicacion_ficha(self, casilla_seleccionada):
        """
        Si es la primer ficha en ser seleccionada, es almacenada en una lista y el color de esta cambia a rojo.
        Si es la segundo ficha (la que se pretende empujar) se valida que si se encuentre al rededor y se pueda empujar.
        Las posicion tanto en la ventana, como en la matriz tablero se actualiza
        :param casilla_seleccionada: Coordenadas [Fila][Columna] de la ficha seleccionada por el jugador
        :return: void
        """
        if len(self.ficha_mover) == 0 and \
                self.obj_tablero.ubicación_fichas[casilla_seleccionada[0]][casilla_seleccionada[1]].get_tipo() != "O":
            self.ficha_mover.append(casilla_seleccionada)
            self.obj_tablero.ubicación_fichas[casilla_seleccionada[0]][casilla_seleccionada[1]].set_color([255, 0, 0])
        elif len(self.ficha_mover) == 1 and casilla_seleccionada != self.ficha_mover[0]:
            """ Se verifica que no se seleccione la misma ficha dos veces"""
            disponible = self.pos_not_available(self.ficha_mover[0], casilla_seleccionada)
            if disponible:
                """ Si se encuentra al rededor, se valida que se pueda empujar y las matrices son actualizdas con
                la nueva posicion"""
                untitled = self.obj_tablero.validar_jugada(self.ficha_mover[0], casilla_seleccionada, True)
                if untitled:
                    self.restaurar_color()
                    if self.turno_jugador1:
                        self.turno_jugador1 = False
                    else:
                        self.turno_jugador1 = True
            else:
                pass
        else:
            pass

    def restaurar_color(self):
        """
        Cuando las fichas son seleccionadas, su color es alterado. Con esta funcion se restaura a sus colores
        predeterminados (Blanco, Negro).
        :return: Void
        """
        for fila in self.obj_tablero.ubicación_fichas:
            for columna in fila:
                if columna == "-":
                    pass
                elif columna.get_tipo() == "O":
                    pass
                else:
                    columna.set_color(columna.get_copy_color())
            self.ficha_mover.clear()

    def pintar_complementos(self):
        pygame.draw.circle(self.window, (0, 0, 0), (80, 120), 30)
        contador = self.fuente.render(str(self.obj_tablero.fichas_negras), True, (255, 255, 255))
        self.window.blit(contador, (70, 105))
        pygame.draw.circle(self.window, (255, 255, 255), (610, 120), 30)
        contador = self.fuente.render(str(self.obj_tablero.fichas_blancas), True, (0, 0, 0))
        self.window.blit(contador, (600, 105))

        if self.obj_tablero.fichas_blancas == 6:
            self.window.blit(self.label_ganador2, (180, 30))
            self.ganador = True
        elif self.obj_tablero.fichas_negras == 6:
            self.window.blit(self.label_ganador1, (180, 30))
            self.ganador = True
        elif self.turno_jugador1:
            self.window.blit(self.label_j1, (260, 30))
        else:
            self.window.blit(self.label_j2, (340, 30))

    def reiniciar(self):
        self.window.blit(self.boton_refresh, (570, 520))
        self.obj_tablero.tablero = []
        self.obj_tablero.ubicación_fichas = []
        self.obj_tablero.inicializar_matriz(self.obj_tablero.tablero)
        self.obj_tablero.read_file(self.ruta)
        self.obj_tablero.cargar_ubicacion_fichas()
        self.obj_tablero.fichas_blancas = 0
        self.obj_tablero.fichas_negras = 0
        self.restaurar_color()
        self.turno_jugador1 = True

    def game(self):
        while True:
            self.window.fill((128, 128, 128))
            self.window.blit(self.boton, (570, 520))
            self.pintar_tablero()
            self.pintar_fichas()
            self.pintar_complementos()

            if not self.turno_jugador1 and not self.ganador:
                IA = InteligenciaArtificial()
                proximo_movimiento = IA.create_tree(self.obj_tablero.tablero)
                untitled = self.obj_tablero.validar_jugada(proximo_movimiento[0], proximo_movimiento[1], True)
                if untitled:
                    self.restaurar_color()
                    if self.turno_jugador1:
                        self.turno_jugador1 = False
                    else:
                        self.turno_jugador1 = True

            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.pos[0] in range(580, 650) and event.pos[1] in range(530, 600):
                        self.reiniciar()
                    elif event.button == 1 and not self.ganador:
                        """ Para optimizar el recorrido en la matriz, se identifica que Fila del tablero fue seleccionada
                            basado en la coordenada (y) en la que fueron pintados los circulos (su centro)"""
                        y = 120
                        for fila in range(9):
                            if event.pos[1] in range(y - 25, y + 25):
                                """ Si fila_seleccionada retorna una lista vacia, significa que el clic del mouse se hizo
                                fuera de alguna ficha o del tablero"""
                                """print("Fila {}".format(fila))"""
                                ubicacion = self.ficha_seleccionada(fila, event.pos[0])
                                if len(ubicacion) != 0:
                                    self.almacenar_ubicacion_ficha(ubicacion)
                            y += 60

                    elif event.button == 3 and not self.ganador:
                        """ Si el clic es derecho, se restaurar los colores de las fichas y se quita la seleccion de las 
                        fichas"""
                        self.restaurar_color()
                pygame.display.update()

    def main(self, ruta):
        self.ruta = ruta
        self.obj_tablero.inicializar_matriz(self.obj_tablero.tablero)
        self.obj_tablero.read_file(ruta)
        self.obj_tablero.cargar_ubicacion_fichas()
        self.game()
