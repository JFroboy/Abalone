from tkinter.filedialog import askopenfilename
from tkinter import *
from GUI import GUI
from game import InteligenciaArtificial


def main():
    Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
    filename = askopenfilename()  # show an "Open" dialog box and return the path to the selected file
    obj = GUI()
    obj.main(filename)
main()