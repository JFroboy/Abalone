

class Ficha:

    def __init__(self, tipo, x, y, color, copy_color):
        self.tipo = tipo
        self.x = x
        self.y = y
        self.color = color
        self.copy_color = copy_color

    def get_tipo(self):
        return self.tipo

    def get_coordenadas(self):
        return [self.x, self.y]

    def get_color(self):
        return self.color

    def get_copy_color(self):
        return self.copy_color

    def set_coordenadas(self, new_coordenadas):
        self.x = new_coordenadas[0]
        self.y = new_coordenadas[1]

    def set_color(self, new_color):
        self.color = new_color

    def set_tipo(self, new_tipo):
        self.tipo = new_tipo

    def seleccionada(self, pos_x):
        """
        Se valida que el clic del mouse se hizo en el interior de la ficha (circulo). Basado en un intervalo a partir
        del centro del circulo
        :param pos_x: Coordenada en X del evento del mouse
        :return: Boolean
        """
        if pos_x in range(self.x-25, self.x+25):
            return True
        else:
            return False