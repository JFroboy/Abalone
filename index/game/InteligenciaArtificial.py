from game import Tablero


class InteligenciaArtificial:

    tablero_costos = []

    def cargar_tablero_costos(self):
        """
        Lee el txt que contiene el valor de cada "casilla" y lo almacena en la matriz tablero_costos
        :return: void
        """
        archivo = open("../../Abalone/resources/tableros/matriz_costos.txt", "r")
        for fila in archivo.readlines():
            self.tablero_costos.append(fila[:-1])

    def calcular_heuristica(self, tablero, tipo_ficha):
        """
        Se calcula la heuristica del movimiento, teniendo en cuenta la utilidad que se tiene al ubicar las fichas
        en determinada posicion.
        :param tablero: tablero del nodo que se esta expandiendo despues de aplicar un determinado movimiento
        :param tipo_ficha: fichas que estan haciendo la jugada
        :return: valor de la heuristica
        """

        """ Se elimina el tipo de ficha que esta realizando el movimiento """
        ficha_defensor = ["B", "N", 0]
        ficha_defensor.remove(tipo_ficha)

        """ Utilidad de las fichas que realizan el movimiento """
        heuristica_positiva = 0

        """ Utilidad de las fichas que se estan defendiendo"""
        heuristica_negativa = 0

        fichas_atacante = 0

        """ Se recorre el tablero de la jugada y cada que encuentra una ficha (ya sea aliada o enemiga) va aumentando 
         su utilidad.
            Se lleva un conteo de las fichas ya que perder una ficha tiene un 'penalizacion de 5puntos por cada una'"""
        for fila in range(9):
            for columna in range(9):
                if tablero[fila][columna] == tipo_ficha:
                    heuristica_positiva += int(self.tablero_costos[fila][columna])
                    fichas_atacante += 1
                elif tablero[fila][columna] == ficha_defensor[0]:
                    heuristica_negativa += int(self.tablero_costos[fila][columna])
                    ficha_defensor[1] += 1
        heuristica_positiva = heuristica_positiva - ((14 - fichas_atacante) * 5)
        heuristica_negativa = heuristica_negativa - ((14 - ficha_defensor[1]) * 5)
        heuristica_total = (heuristica_positiva - heuristica_negativa)
        print("Heuristica: ", heuristica_total)
        return heuristica_total

    def pos_fichas_dispobibles(self, tablero_padre, tipo_ficha):
        """
        Realiza la busqueda de un determinado tipo de fichas.
        :param tablero_padre: tablero del nodo a expandir
        :param tipo_ficha: N (Ficha negra) o B (Ficha Blanca)
        :return: lista de las posiciones de las fichas en el tablero
        """
        lista_fichas = []
        for i, fila in enumerate(tablero_padre):
            for j, columna in enumerate(fila):
                if columna == tipo_ficha:
                    lista_fichas.append([i, j])
        return lista_fichas

    def posible_jugada(self, pos_ficha):
        """
        Las fichas se pueden desplazar a 1 de las 6 casillas que la rodean.
        Esta funcion valida que no se trate de desplazar a coordenadas que sobrepasan el limite de la matriz
        :param pos_ficha: pos de Ficha a mover
        :return: lista de las casillas a las que puede desplazarse
        """
        lista_posibles = [(pos_ficha[0]-1, pos_ficha[1]), (pos_ficha[0]-1, pos_ficha[1]+1), (pos_ficha[0], pos_ficha[1]-1),
                          (pos_ficha[0], pos_ficha[1]+1), (pos_ficha[0]+1, pos_ficha[1]-1), (pos_ficha[0]+1, pos_ficha[1])]
        lista_permitidas = []
        for val in lista_posibles:
            if val[0] in range(9) and val[1] in range(9):
                lista_permitidas.append(val)
                pass
            else:
                pass
        return lista_permitidas

    def copy_tablero(self, origen, destino):
        for fila in range(9):
            for columna in range(9):
                destino[fila][columna] = origen[fila][columna]

    def incializar_matriz(self):
        matriz = []
        for val in range(9):
            fila = [0, 0, 0, 0, 0, 0, 0, 0, 0]
            matriz.append(fila)
        return matriz

    def create_tree(self, tablero_actual):
        """
        Crea el arbol (profundidad 2)de decisiones.
        La profundidad 2 indica que solo se trata de predecir UNA jugada proxima del contrincante
        :param tablero_actual: tablero con el ultimo movimiento del jugador
        :param fichas_blancas:
        :param fichas_negras:
        :return:
        """
        self.cargar_tablero_costos()
        arbol = []
        obj_tablero = Tablero()
        raiz = {"padre": None, "tablero": tablero_actual, "valor": -100, "ficha_movida": [], "turno": "N",
                "algoritmo": "MAX"}
        arbol.append(raiz)
        i = 0
        niveles = [0]
        while i < 2:
            niveles.append(len(arbol))
            for index in range(niveles[i], len(arbol)):
                tablero_padre = self.incializar_matriz()
                self.copy_tablero(arbol[index].get("tablero"), tablero_padre)
                fichas = self.pos_fichas_dispobibles(tablero_padre, arbol[index].get("turno"))
                for ficha in fichas:
                    destino = self.posible_jugada(ficha)
                    for casilla in destino:
                        obj_tablero.tablero = []
                        obj_tablero.inicializar_matriz(obj_tablero.tablero)
                        self.copy_tablero(tablero_padre, obj_tablero.tablero)
                        if obj_tablero.validar_jugada(ficha, casilla, False):
                            nodo = {"padre": index,
                                    "tablero": [],
                                    "valor": 0,
                                    "ficha_movida": [ficha, casilla],
                                    "turno": "",
                                    "algoritmo": ""}
                            aux = self.incializar_matriz()
                            self.copy_tablero(obj_tablero.tablero, aux)
                            nodo["tablero"] = aux
                            if arbol[index].get("turno") == "N":
                                nodo["turno"] = "B"
                                nodo["algoritmo"] = "MIN"
                                nodo["valor"] = 100
                            else:
                                nodo["turno"] = "N"
                                nodo["algoritmo"] = "MAX"
                                nodo["valor"] = -100
                            if len(niveles) == 3:
                                nodo["valor"] = self.calcular_heuristica(obj_tablero.tablero, arbol[index].get("turno"))
                            arbol.append(nodo)
            i += 1
        return self.mini_max(arbol, niveles)

    def eliminar_hijos(self, arbol, indice):
        i = indice + 1
        while True:
            if i < len(arbol):
                if arbol[i-1].get("padre") == arbol[i].get("padre"):
                    arbol.remove(arbol[i])
                else:
                    break
            else:
                break

    def mini_max(self, arbol, niveles):
        proxima_jugada = []
        extremos = [-100, 100]
        stop = len(arbol)
        nodo = niveles[2]
        while nodo < stop:
            padre = arbol[(arbol[nodo].get("padre"))]
            if padre.get("valor") in extremos:
                if nodo-1 > niveles[2]:
                    if arbol[(arbol[nodo-1].get("padre"))].get("valor") > arbol[0].get("valor"):
                        arbol[0]["valor"] = arbol[(arbol[nodo - 1].get("padre"))].get("valor")
                        proxima_jugada.append(arbol[(arbol[nodo - 1].get("padre"))].get("ficha_movida"))
                        arbol[(arbol[nodo].get("padre"))]["valor"] = arbol[nodo].get("valor")
                        if arbol[nodo].get("valor") <= arbol[0].get("valor"):
                            self.eliminar_hijos(arbol, nodo)
                            stop = len(arbol)
                        else:
                            pass
                    elif arbol[nodo].get("valor") <= arbol[0].get("valor"):
                        self.eliminar_hijos(arbol, nodo)
                        stop = len(arbol)
                    else:
                        arbol[arbol[nodo].get("padre")]["valor"] = arbol[nodo].get("valor")
                else:
                    arbol[arbol[nodo].get("padre")]["valor"] = arbol[nodo].get("valor")
            else:
                if arbol[0].get("valor") in extremos:
                    if arbol[arbol[nodo].get("padre")].get("valor") > arbol[nodo].get("valor"):
                        arbol[arbol[nodo].get("padre")]["valor"] = arbol[nodo].get("valor")
                    else:
                        pass
                elif arbol[nodo].get("valor") <= arbol[0].get("valor"):
                        self.eliminar_hijos(arbol, nodo)
                        stop = len(arbol)

                elif arbol[arbol[nodo].get("padre")].get("valor") > arbol[nodo].get("valor"):
                        arbol[arbol[nodo].get("padre")]["valor"] = arbol[nodo].get("valor")
                else:
                    pass
            nodo += 1
        print("Valor Raiz", arbol[0].get("valor"))
        return proxima_jugada[len(proxima_jugada)-1]