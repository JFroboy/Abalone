from game import Ficha

class Tablero:
    tablero = []
    ubicación_fichas = []
    fichas_negras = 0
    fichas_blancas = 0

    def read_file(self, ruta):
        """
        Se lee el TxT para obtener las posiciones iniciales de las fichas
        :return: void
        """
        archivo = open(ruta, "r")
        aux = 0
        for i, line in enumerate(archivo.readlines()):
            """ Debido a la forma del tablero, hay casillas de la matriz que se 'anulan'"""
            if i < 5:
                """ De la fila 1 a la 5 del tablero hexagonal, las fichas son ubicadas  en la matriz de la sgt manera:
                -> La fila para almacenar las fichas es a partir de #4 (aumenta basada en la linea del tablero) y 
                columna #1.
                -> Cuando se ubica una ficha la siguiente va en la posicion[fila-1][columna+1] formando una diagonal"""
                fila = 4
                for columna, value in enumerate(line):
                    if value == "B" or value == "N":
                        self.tablero[i+fila][columna] = value
                    fila -= 1
            else:
                """Las 4 filas restantes del tablero hexagonal (6,7,8,9). Siguen la sgt secuencia
                -> La fila inicial siempre es la 8 y va disminuyendo a medida que se ubican las fichas.
                -> Respecto a la columna (la linea #6 inicia en la columna 1) y para las demás lineas va aumentando en 1
                -> La ubicacion de las fichas tambien forma una diagonal
                """
                fila = 8
                aux += 1
                for columna, value in enumerate(line):
                    if value == "B" or value == "N":
                        self.tablero[fila][columna+aux] = value
                    fila -= 1

    def inicializar_matriz(self, table):
        """
        Para cargar las fichas correctamente se iniciliza el tablero con 'O' para casilla vacia y '-' las celdas que
        estan inhabilitadas por la forma del tablero del Abalone
        :param table: Matriz[9][9]
        :return: void
        """
        not_available = [4, 8]

        for fila in range(9):
            linea = []
            if fila < 5:
                for value in range(9):
                    if value < not_available[0]:
                        linea.append("-")
                    else:
                        linea.append("O")
                not_available[0] -= 1
            else:
                for value in range(9):
                    if value >= not_available[1]:
                        linea.append("-")
                    else:
                        linea.append("O")
                not_available[1] -= 1
            table.append(linea)

    def cargar_ubicacion_fichas(self):
        """
        Se carga una segunda matriz que contiene objetos de tipo Ficha (tipo, posx, posy, color, color_copy).
        La funcion de esta matriz es el de a pintar las fichas en la ventana.
        El recorrido de la matriz es igual al del observado en la funcion  inicializar_matriz().
        Cada que se encuentra un 'O' | 'N' | 'B' se crea un objeto de tipo ficha con los respectivos atributos
        :return: Void
        """
        self.inicializar_matriz(self.ubicación_fichas)
        pos_inicial_x = 242
        pos_y = 120
        negro = (0, 0, 0)
        blanco = (255, 255, 255)
        cuadros_fila = 4
        aux = 1
        for i in range(9):
            if i < 5:
                fila = 4
                cuadros_fila += 1
                pos_inicial_x -= 32
                pos_x = pos_inicial_x
                for columna in range(cuadros_fila):
                    if self.tablero[i + fila][columna] == "B":
                        ficha = Ficha("B", pos_x, pos_y, blanco, blanco)
                        self.ubicación_fichas[i + fila][columna] = ficha
                    elif self.tablero[i + fila][columna] == "N":
                        ficha = Ficha("N", pos_x, pos_y, negro, negro)
                        self.ubicación_fichas[i + fila][columna] = ficha
                    else:
                        ficha = Ficha("O", pos_x, pos_y, "No Aplica", "No Aplica")
                        self.ubicación_fichas[i + fila][columna] = ficha
                    pos_x += 65
                    fila -= 1
                pos_y += 60
            else:
                fila = 8
                cuadros_fila -= 1
                pos_inicial_x += 32
                pos_x = pos_inicial_x
                for columna in range(cuadros_fila):
                    if self.tablero[fila][columna + aux] == "B":
                        ficha = Ficha("B", pos_x, pos_y, blanco, blanco)
                        self.ubicación_fichas[fila][columna + aux] = ficha
                    elif self.tablero[fila][columna + aux] == "N":
                        ficha = Ficha("N", pos_x, pos_y, negro, negro)
                        self.ubicación_fichas[fila][columna + aux] = ficha
                    else:
                        ficha = Ficha("O", pos_x, pos_y, "No Aplica", "No Aplica")
                        self.ubicación_fichas[fila][columna + aux] = ficha
                    fila -= 1
                    pos_x += 65
                aux += 1
                pos_y += 60

    def actualizar_matrices(self, origen, destino, pintar):
        """
        Se realiza el intercambio de posicion de dos fichas:
        -> En la matriz tablero solo se intercambian de posicion las letras respectivas de cada ficha ('O' - 'B' - 'N')
        -> En la matriz ubicacion_fichas a parte de intercambiae de posicion los objetos fichas, se debe hacer un
        intercambio de coordenas para que se vea reflejado el moviendo en pantalla
        :param origen: posicion (fila, columna) de una determinada ficha
        :param destino: posicion (fila, columna) de una determinada ficha
        :param pintar: boolean, indica si se debe o no actualizar la matriz de ubicacion de fichas
        :return:
        """

        """ Se almacenan las letras (O - N - B) que representan las fichas en la matriz tablero """
        ficha_origen = self.tablero[origen[0]][origen[1]]
        ficha_destino = self.tablero[destino[0]][destino[1]]
        self.tablero[origen[0]][origen[1]] = ficha_destino
        self.tablero[destino[0]][destino[1]] = ficha_origen

        if pintar:
            """ Se almacenan las fichas que serán movidas de posicion """
            ubicacion_origen = self.ubicación_fichas[origen[0]][origen[1]]
            ubicacion_destino = self.ubicación_fichas[destino[0]][destino[1]]

            """ Se almacenan las coordenadas en la que se encuentran pintadas las fichas en patanalla """
            coordenadas_origen = self.ubicación_fichas[origen[0]][origen[1]].get_coordenadas()
            coordenadas_destino = self.ubicación_fichas[destino[0]][destino[1]].get_coordenadas()

            """ Se hace el intercambio """
            self.ubicación_fichas[origen[0]][origen[1]].set_coordenadas(coordenadas_destino)
            self.ubicación_fichas[destino[0]][destino[1]].set_coordenadas(coordenadas_origen)
            self.ubicación_fichas[origen[0]][origen[1]] = ubicacion_destino
            self.ubicación_fichas[destino[0]][destino[1]] = ubicacion_origen
        else:
            pass

    def intercambiar_posicion(self, secuencia, pintar):
        """
        De acuerdo a los diferentes escenarios que se puedan presentar, se realiza el intercambio correcto de las fichas.
        :param secuencia: lista de fichas que se van a mover
        :return: void
        """
        num = len(secuencia)
        if self.tablero[secuencia[num-1][0]][secuencia[num-1][1]] == 'O':
            if self.tablero[secuencia[0][0]][secuencia[0][1]] \
                    != self.tablero[secuencia[num-2][0]][secuencia[num-2][1]]:
                for val in secuencia:
                    """ Se recorre la secuencia de fichas, hasta hallar una cambio de tipo de fichas (B:N) """
                    if self.tablero[val[0]][val[1]] != self.tablero[secuencia[0][0]][secuencia[0][1]]:
                        """ La primer ficha del defensor, es intercambiada por la primera del atacante"""
                        self.actualizar_matrices(val, secuencia[0], pintar)
                        """ Nuevamente la ficha del defenseron es intercambiada por la ultima de la 
                        secuencia (Casilla. Vacia) """
                        self.actualizar_matrices(secuencia[0], secuencia[num - 1], pintar)
                        break
            else:
                """ Si todas las fichas son de un mismo tipo, solo se intercambia la primera por la ultima (casilla vacia)"""
                self.actualizar_matrices(secuencia[0], secuencia[num - 1], pintar)
        else:
            """ Si al final de la secuencia no hay una casilla en blanco, indica que una ficha será expulsada 
            del tablero"""
            for val in secuencia:
                """ Se recorre la secuencia de fichas, hasta hallar una cambio de tipo de fichas (B:N).
                El intercambio se realiza de la siguiente manera"""
                if self.tablero[val[0]][val[1]] != self.tablero[secuencia[0][0]][secuencia[0][1]]:
                    """ La primer ficha del defensor, es intercambiada por la primera del atacante"""
                    self.actualizar_matrices(val, secuencia[0], pintar)
                    """ Luego es modificado el tipo de la ficha por 'O' para simular la expulsion  de la ficha """
                    if self.tablero[secuencia[0][0]][secuencia[0][1]] == "N":
                        self.fichas_negras += 1
                    else:
                        self.fichas_blancas += 1
                    self.tablero[secuencia[0][0]][secuencia[0][1]] = "O"
                    self.ubicación_fichas[secuencia[0][0]][secuencia[0][1]].set_tipo("O")
                    break

    def validar_jugada(self, pos_inicial, pos_destino, pintar):
        """
        Se valida si la ficha inicial puede ubicarse en la posicion destino.
        Para esto se almacenan las fichas que estan en una determinada direccion hasta encontrar (una casilla vacia o que
        se termine el tablero).
        Para determinar el cambio de fila y columna de cada iteracion se realiza de la siguiente forma:
            -> Fila = (pos_destino[0] - pos_inicial[0]) y Columna = (pos_destino[1] - pos_inicial[1])
        De esta forma se consigue un iteracion en una sola direccion, ya sea que la ficha este a (0-45-135-180-225-315)
        grados de la ficha inicial
        :param pos_inicial: int[] con la posicion (fila, columna) en las matrices
        :param pos_destino: int[] con la posicion (fila, columna) en las matrices
        :return:
        """
        if self.tablero[pos_inicial[0]][pos_inicial[1]] == "B" and \
                self.tablero[pos_destino[0]][pos_destino[1]] == "N":
            """ Para que desplazar una ficha del jugador enemigo se debe tener superioridad numerica"""
            return False
        elif self.tablero[pos_inicial[0]][pos_inicial[1]] == "N" and \
                self.tablero[pos_destino[0]][pos_destino[1]] == "B":
            return False
        elif self.tablero[pos_destino[0]][pos_destino[1]] == "-":
            return False
        else:
            """ Si las dos fichas iniciales son del mismo jugador o la segunda seleccionada es una casilla en blanco """
            num_atacante = 1
            num_defensor = 0
            atacante = self.tablero[pos_inicial[0]][pos_inicial[1]]
            secuencia = [pos_inicial]
            iterador_fila = pos_destino[0] - pos_inicial[0]
            iterador_columna = pos_destino[1] - pos_inicial[1]
            fila = pos_destino[0]
            columna = pos_destino[1]
            while fila in range(0, 9) and columna in range(0, 9):
                """ En cada iteracion se almacenan las fichas en un lista(secuencia), el ciclo termina cuando se 
                encuentre con una casilla vacia, una casilla no valida o se termine la matriz de 9x9.
                Ademas se lleva un conteo de los tipos de fichas"""
                if self.tablero[fila][columna] == "O":
                    secuencia.append([fila, columna])
                    break
                elif self.tablero[fila][columna] == "-":
                    break
                else:
                    secuencia.append([fila, columna])
                    if self.tablero[fila][columna] == atacante:
                        num_atacante += 1
                    else:
                        num_defensor += 1
                fila += iterador_fila
                columna += iterador_columna

            if num_defensor == 0 and self.tablero[secuencia[len(secuencia)-1][0]][secuencia[len(secuencia)-1][1]] != "O":
                return False
            elif num_defensor < num_atacante <= 3:
                """ El numero maximo de fichas que un jugador puede mover es de 3 y para poder empujar las fichas del
                adversario debe existir superioridad numerica"""
                self.intercambiar_posicion(secuencia, pintar)
                return True
            else:
                """ Si no cumple lo anterior, la jugada no se puede realizar """
                return False